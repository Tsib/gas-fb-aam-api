function GraphAPIbatch() {

  ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Ad URLs");
  ssOutputTraits = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaign Traits");
  market = ssInput.getRange(1, 2).getValue();
  region = ssInput.getRange(2, 2).getValue();
  traitLevel = ssInput.getRange(3, 2).getValue();
  
  
  //clear output rows
  ssOutput.getRange(3, 1, 5000, 29).clear({contentsOnly: true });
  ssOutputTraits.getRange(3, 1, 5000, 29).clear({contentsOnly: true });
  
  rowsInput = ssInput.getLastRow();
  rO = 3;
  rOcampaign = 3;
  rOtraits = 3;
  
  var DocumentProperties  = PropertiesService.getDocumentProperties();
  FBaccessToken = DocumentProperties.getProperty("FBaccessToken");
  //FB Test app Sandbox Ad Account token
  //FBaccessToken = "EAATVbHqfs20BAFhl8oDsnMCtGeP8LIlP06k6arzouFZAPmZAzhl89QZBSws6Mdw1Od63ifOsK8qO4WAHZCWk5Mz0TTlZBaaYuT56VFXjDjXL0mdsx6eyiLqfkuWaYi0yJbzFPdcZADwlGcKI6e2lxmeZBeYRnI6TRzPIPQIZBrR5I6rZBON48GVNd"
  
  options = {'headers': {'Authorization':'Bearer ' + FBaccessToken }, 'muteHttpExceptions':false}
  optionsBatch = {'method':'POST', 'headers': {'Authorization':'Bearer ' + FBaccessToken }, 'muteHttpExceptions':false}

  //list of campaigns to loop through
  for (rI = 6; rI <= rowsInput; rI++) {
        
   campaignID = ssInput.getRange(rI, 1).getValue();
    if (campaignID != "") {
      ssOutput.getRange(rO, 2).setValue(campaignID)
      brandName = ssInput.getRange(rI, 2).getValue();
      prodCatCode = ssInput.getRange(rI, 5).getValue();
      brandCode = ssInput.getRange(rI, 3).getValue();
      destinationType = ssInput.getRange(rI, 6).getValue();
      socialFolder = ssInput.getRange(rI, 7).getValue();
      FB_IG = ssInput.getRange(rI, 8).getValue();
      getCampaignNameBatch(campaignID, rI, rO, socialFolder, brandName);
      getAdsetsBatch(campaignID, rI, prodCatCode, brandCode, destinationType, socialFolder, FB_IG);  
    }
  }
  
}

  function getCampaignNameBatch(campaignID, rI, rO, socialFolder, brandName) {
  
  //get campaign name
  //https://developers.facebook.com/docs/marketing-api/reference/ad-campaign-group#Reading

  var campaignResponse = JSON.parse( UrlFetchApp.fetch("https://graph.facebook.com/v3.2/" + campaignID + "?fields=name,start_time", options) );
  campaignName = campaignResponse.name;
  startTime = campaignResponse.start_time.split("T")[0];
  //write to Ad sheet to accompany ad details for URL tagging
  ssOutput.getRange(rO, 1).setValue(brandName);
  ssOutput.getRange(rO, 2).setValue(campaignID);
  ssOutput.getRange(rO, 3).setValue(campaignName);  
  rOcampaign++;
    if (traitLevel == "Campaign") { 
      //write to campaign sheet to be used for trait creation
      ssOutputTraits.getRange(rOtraits, 2).setValue(brandName);
      ssOutputTraits.getRange(rOtraits, 3).setValue(campaignID);
      ssOutputTraits.getRange(rOtraits, 4).setValue(campaignName);
      ssOutputTraits.getRange(rOtraits, 9).setValue(socialFolder);
      rOtraits++ }
  }
  
  function getAdsetsBatch(campaignID, rI, prodCatCode, brandCode, destinationType, socialFolder, FB_IG) {
  //get ad set names and IDs within campaign
  var adsetsResponse = JSON.parse( UrlFetchApp.fetch("https://graph.facebook.com/v3.2/" + campaignID + "/adsets?fields=name,id,targeting&limit=100", options) );
  //check if there are too many results which are being paginated and therefore a follow-up request is required
    if ( typeof(adsetsResponse.paging.next) !== "undefined") {
       adsetsResponse = FBpaginatedCalls( adsetsResponse, FBaccessToken );
    }  
    
  var adsets = adsetsResponse.data;
      
  for (var d = 0; d < adsets.length; d++) {


  //write Adset Names and IDs to Ad URLs sheet  
//  ssOutput.getRange(rO, 2).setValue(campaignID)
//  ssOutput.getRange(rO, 3).setValue(campaignName)  
//  ssOutput.getRange(rO, 1).setValue(brandName);
  ssOutput.getRange(rO, 4).setValue( adsets[d].id )
  ssOutput.getRange(rO, 5).setValue( adsets[d].name );
    //write Adset Names and IDs to Trait sheet for group trait creation if the Trait Level is set to AdSet or Ad
    if (traitLevel == "AdSet" || traitLevel == "Ad") {
      ssOutputTraits.unhideColumn( ssOutputTraits.getRange(1, 5, 1, 2) ); 
      ssOutputTraits.hideColumns( 7, 2 ); 
      ssOutputTraits.getRange(rOtraits, 2).setValue(brandName);
      ssOutputTraits.getRange(rOtraits, 3).setValue(campaignID);
      ssOutputTraits.getRange(rOtraits, 4).setValue(campaignName);
      ssOutputTraits.getRange(rOtraits, 5).setValue( adsets[d].id )
      ssOutputTraits.getRange(rOtraits, 6).setValue( adsets[d].name );    
      ssOutputTraits.getRange(rOtraits, 9).setValue(socialFolder);      
      rOtraits++;
    } else {
      ssOutputTraits.hideColumns( 5, 4 ); 
    } 
    
    var audience = adsets[d].targeting
    var gender = audience.genders;
    var ageMin = audience.age_min;
    var ageMax = audience.age_max;

//translate FB gender code into M/F/A
      switch(gender) {
    case "0":
       gender = "A";
        break;
    case "1":
        gender = "M";
        break;
    case "2":
        gender = "F";
        break;
   default:
        gender = "A";
        break;
      }
    

  
  //array to store Ad IDs for batch request
  //https://developers.facebook.com/docs/marketing-api/reference/adgroup#Reading
  var batchAds = [];
  
  var adsResponse = JSON.parse( UrlFetchApp.fetch("https://graph.facebook.com/v3.2/"+adsets[d].id+"/ads?fields=name,id,status&limit=100", options) );  
    
     //check if there are too many results which are being paginated and therefore a follow-up request is required
    if ( typeof(adsResponse.paging.next) !== "undefined") {
       adsResponse = FBpaginatedCalls( adsResponse, FBaccessToken );
    }  
   
   var ads = adsResponse.data; 
   
    for (var D =0; D < ads.length; D++) {
      
      //write Ad Name, ID to sheet 
      ssOutput.getRange(rO+D, 6).setValue( ads[D].id )
      ssOutput.getRange(rO+D, 7).setValue( ads[D].name )
      //write Ad Names and IDs to Trait sheet for group trait creation if the Trait Level is set to Ad
      if (traitLevel == "Ad") {
        ssOutputTraits.unhideColumn( ssOutputTraits.getRange(1, 5, 1, 4) );
        ssOutputTraits.getRange(rOtraits, 7).setValue( ads[D].id )
        ssOutputTraits.getRange(rOtraits, 8).setValue( ads[D].name );
        rOtraits++;
      }
      
      //compose batch Ads request object
       batchAds.push( {'method': 'GET','relative_url': 'v3.2/'+ ads[D].id +"/adcreatives?fields=name,image_url,url_tags,object_story_spec,asset_feed_spec"} )

    }

 
      //get AdCreatives in Ads from BATCH request
      var adCreativesResponse = JSON.parse( UrlFetchApp.fetch("https://graph.facebook.com?batch="+ encodeURIComponent( JSON.stringify(batchAds) ) , optionsBatch));   
                                                        
      
      for (var C = 0; C < adCreativesResponse.length; C++) {
      
        var adCreative = JSON.parse( adCreativesResponse[C].body).data[0] 
        var objectStorySpec = adCreative.object_story_spec;
        var adType = "";
        var destinationURL = "";
        var  AAMinURL = "";
        
 //check for link data based on ad format
        if (typeof(objectStorySpec) == "undefined") {
          //for Awareness image posts with no CTA link
           destinationURL = "No CTA link in ad";
            AAMinURL = "N/A";
          
        } else if (objectStorySpec['link_data'] !== undefined) {
          //for carousel ads. Loop through all cards and add links to comma delimited list
            if (objectStorySpec['link_data'].child_attachments !== undefined) {
              for (u in objectStorySpec['link_data'].child_attachments) {
                destinationURL += objectStorySpec['link_data'].child_attachments[u].link + '::';     
              }
            //if the carousel includes an optional See More card at the end also append that link
              if ( typeof(objectStorySpec.link_data.link) !== "undefined" ) {
                destinationURL+= objectStorySpec.link_data.link + "::";
              }
              destinationURL = destinationURL.substring(0, destinationURL.length-2);
              //only check the first card for AAM tracking!!
              AAMinURL = objectStorySpec['link_data'].child_attachments[0].link.match("demdex.net") !== null ? true : false;
              adType = "carousel";
            
            } else {
              if (objectStorySpec.link_data.link.indexOf("fb.com") < 0 ) {
                //for image ads
           destinationURL = objectStorySpec.link_data.link;
           AAMinURL = destinationURL.match("demdex.net") !== null ? true : false;
           adType = "image";
                
              } else {
                //for canvas/instant experience ads
              destinationURL = "Instant Experiences not supported";
              AAMinURL = "UNSUPPORTED";
                adType = "Instant Experience"
              }
            }
          
        } else if (objectStorySpec['video_data'] !== undefined) {
          //for video ads
           destinationURL = objectStorySpec.video_data.call_to_action.value.link;
          AAMinURL = destinationURL.match("demdex.net") !== null ? true : false;
           adType = "video";
       
        } else { 
        //for ads with multiple creative assets in them, eg different videos for different placements
           destinationURL = adCreative.asset_feed_spec.link_urls[0].website_url;
          AAMinURL = destinationURL.match("demdex.net") !== null ? true : false;
           adType = "multiAsset";
        }
      
      //write Ad image, URLs to sheet 
      ssOutput.getRange(rO, 8).setValue( adCreative.image_url || "" );
      ssOutput.getRange(rO, 9).setValue( destinationURL );
     
        //check if URL shortener is being used that might conceal AAM redirect
      if (destinationURL.match("bit.ly") !== null) { 
        var longURL = expandBitly(destinationURL)
        ssOutput.getRange(rO, 10).setValue( longURL );
        AAMinURL = longURL.match("demdex.net") !== null ? true : false;
      }
      
      ssOutput.getRange(rO, 1).setValue(brandName);
      ssOutput.getRange(rO, 2).setValue(campaignID);
      ssOutput.getRange(rO, 3).setValue(campaignName);
      ssOutput.getRange(rO, 4).setValue( adsets[d].id )
      ssOutput.getRange(rO, 5).setValue( adsets[d].name )
      ssOutput.getRange(rO, 11).setValue( AAMinURL );
      
      //write Campaign and Adset details for Ad to Trait sheet for group trait creation if the Trait Level is set to Ad
      if (traitLevel == "Ad") {
        ssOutputTraits.getRange(rOtraits, 2).setValue(brandName);
        ssOutputTraits.getRange(rOtraits, 3).setValue(campaignID);
        ssOutputTraits.getRange(rOtraits, 4).setValue(campaignName);
        ssOutputTraits.getRange(rOtraits, 5).setValue( adsets[d].id )
        ssOutputTraits.getRange(rOtraits, 6).setValue( adsets[d].name )
        ssOutputTraits.getRange(rOtraits, 9).setValue(socialFolder);
        rOtraits++
      }
        
        var taggedURL = ""
        //compose tagged URL if AAM tracking is missing
        if (AAMinURL == false && adType !== "carousel") {
          taggedURL = tagURL(region, market, adType, destinationURL, campaignName, gender, ageMin, ageMax, prodCatCode, brandCode, destinationType, FB_IG);
        } else if (AAMinURL == false && adType == "carousel") {
          //if the ad is a carousel then run the tagging function multiple times and add URLs in a double colon delimited string
          var carouselURLs = destinationURL.split("::");
          for (cU in carouselURLs) {
            taggedURL+= tagURL(region, market, adType, carouselURLs[cU], campaignName, gender, ageMin, ageMax, prodCatCode, brandCode, destinationType, FB_IG) + "::";
               }
           taggedURL = taggedURL.substring(0, taggedURL.length-2);
        }
          
        //write tagged URL to sheet for checking as well as object_story_spec for later upload
        ssOutput.getRange(rO, 12).setValue( taggedURL );
          
               
        //write objectStorySpec in sheet
        ssOutput.getRange(rO, 17).setValue( JSON.stringify( adCreative ) );
        ssOutput.getRange(rO, 16).setValue( adType );
      
   
        
        
     //move on to the next row 
      rO++;  
           //close AdCreative loop
           }
      
     //close Ads loop 
         
    
    //close AdSet loop
  }
  
}



//recursive function to continue quering paginated results until reaching the last page of data
function FBpaginatedCalls(response, FBaccessToken) {

  var results = JSON.parse( UrlFetchApp.fetch( decodeURIComponent(response.paging.next)+ "&access_token="+FBaccessToken) );
  //append the original data to the newly pulled data
  results.data = results.data.concat(response.data);
  
  //check if there is another page of results to be pulled
  if ( typeof(results.paging) !== "undefined" && typeof(results.paging.next) !== "undefined") {
  var results = FBpaginatedCalls(results, options);
  }

  return results;

}