//function to copy folder refs from the 1. Master Sizmek-AAM Automation logs sheet which pulls them daily
//this function is only used to copy the ref sheet **daily** to the Master FB Templates sheet.
//The Folders Refs get copied to local instances from the Master Templates only when the addon is first installed during onboarding, or when a user runs the Update Template Sheets command.

//This function is scheduled to run daily, at 7-8AM UK time, after the Sizmek sheet has pulled new folders from the API.
function updateFolderRefs() {
  
  //the FB Master Template sheet
  var ss = SpreadsheetApp.openById("1wKuH3lriy_NOWxFXumkXfVu9wgUzKNuPsYTwLPv8PSY");
  ss.deleteSheet(ss.getSheetByName("Trait Folder Lookup IDs") );
  
  var sizmekRef = SpreadsheetApp.openById("1LyDRWcrAMWqj8-MarckmS1BAcHtvOqn5iHVb8aZrvk0");
  var ssFolders = sizmekRef.getSheetByName("Folder lookups").copyTo(ss).setName("Trait Folder Lookup IDs");
  
  
  
}
