//this file contains the function to parse a FB Campaign name into an AAM Trait name based on the Naming convention used in the market

//campaign level trait
function parseCampaignName(region, market, brand, FBcampaignName) {
  
  var fragments = "";
  var campaign = "";
  var launch = "";
  var objective = "";
  var code = "";
  var L5 = "";
  
  if (region == "LATAM") {
  //case for Cadreon naming convention across LATAM
    
    fragments = FBcampaignName.split(" | ");
    //determine whether it's a global campaign in which case there is an extra position for the L_ at the start because whoever made this convention IS A FLIPPING GENIUS
   // (fragments[0] == "L" ) ? offset = 1 : offset = 0;
    campaign = fragments[1];
    launch = fragments[2];
    objective = "" //fragments[6];
    //not using code for LATAM
    code = "";
    
    
  } else if (region == "NA") {
    //Case for MS naming convention in US and CA
    fragments = FBcampaignName.split("_");
    campaign = "";
    code = "-CODE";
    
  } else if ( market.match("DE|CH|AT|UK|TR") ) {
    //case for MS naming convention from the DRAW generator tool in Europe - using market names instead of region in case there is an exception
    fragments = FBcampaignName.split("_");
    (fragments[1] == "L" ) ? offset = 2 : offset = 0;
    campaign = fragments[0 + offset];
    launch = fragments[1 + offset];
    //code = "-" + fragments[2 + offset] || "";
    
  } else if ( market.match("SA|AE") ) {
    //case for NAME - Magna's convention: BRAND | PRODUCTTYPE | FB/IG | SUBREGION | OBJECTIVE | campaign name | XXXXX | BO# 347398 | Camp Custom (used for date)
    fragments = FBcampaignName.split(" | ");
    //determine whether it's a global campaign in which case there is an extra position for the L_ at the start because whoever made this convention IS A FLIPPING GENIUS
   // (fragments[0] == "L" ) ? offset = 1 : offset = 0;
    campaign = fragments[5];
    launch = fragments[8];
    objective = fragments[4];
    var subregion = fragments[3];
    //add the Object to the Campaign name to distinguish similar campaigns with different objectives
    campaign = campaign + "-" + objective
    
  }
  
  var traitName = market + "_" + brand + "_SocialFacebookInstagram_Click_Campaign_" + campaign + "-" + launch //+ code;
  return traitName;
  
}

//placement level trait