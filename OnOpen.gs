//function to run on opening the sheet which creates the UI menus
function onOpen() {
  
var ui = SpreadsheetApp.getUi();
ui.createMenu('UC Automation')
                .addSubMenu( ui.createMenu("Facebook").addItem('Get Campaign Data', 'GraphAPIbatch').addItem("Upload Tagged URLs", "uploadURL")
                            .addSeparator().addItem('Authorise','showSidebar').addItem('Logout API from FB account', 'FBlogout') )
                .addSeparator()
                .addSubMenu( ui.createMenu("AAM").addItem('Create Campaign Traits', 'AAMtraits')
                              .addItem("Create Group Traits", "createGroupTraits")
                            //.addItem("Create Segments", "AAMsegments")
                            .addSeparator()
                            .addItem("Set AAM login", "loginAAM") )
                .addSeparator()
                .addSubMenu( ui.createMenu("AAM-FB Audience Sync").addItem('Get Segments mapped to FB', 'getDestinationMappings').addItem("Create FB Audiences", "syncSegments") )
                .addSeparator()            

//.addSubMenu( ui.createMenu("Schedule").addItem("Create daily sync schedule", "addSchedule").addItem("Delete daily schedule", "deleteSchedule") )
//.addSeparator()
.addItem("Update All Template sheets", "updateTemplate")
.addItem("Update Only Reference sheets", "updateRefs")
                .addToUi();  
  
//function to run and display updates for new versions
//runUpdate();  
  
  
}

//function to run when first installing the AddOn for a market, which 
function onInstall() {

  //the ID of the global template spreadsheet where sheets will be copied from
  var masterTemplate = SpreadsheetApp.openById("1wKuH3lriy_NOWxFXumkXfVu9wgUzKNuPsYTwLPv8PSY");
  
  
  var ss = SpreadsheetApp.getActive();
  ss.rename("UC FAST - ADDMARKETNAME");
  //transfer ownership of the sheet with the global team
   DriveApp.getFileById(ss.getId()).setOwner("audiencemanager.prm@gmail.com")

  
  //Copy all the necessary template sheets
  masterTemplate.getSheetByName("Guide").copyTo(ss).setName("Guide");
  var INPUT = masterTemplate.getSheetByName("INPUT - Options").copyTo(ss).setName("INPUT - Options");
  masterTemplate.getSheetByName("OUTPUT - Ad URLs").copyTo(ss).setName("OUTPUT - Ad URLs");
  masterTemplate.getSheetByName("OUTPUT - Campaign Traits").copyTo(ss).setName("OUTPUT - Campaign Traits");
  masterTemplate.getSheetByName("AAM Mapping-FB Audience Sync").copyTo(ss).setName("AAM Mapping-FB Audience Sync");
  var BRANDCODES = masterTemplate.getSheetByName("Brand & ProdCat codes").copyTo(ss).setName("Brand & ProdCat codes").hideSheet()
  var DSIDs = masterTemplate.getSheetByName("Data Source IDs").copyTo(ss).setName("Data Source IDs").hideSheet();
  masterTemplate.getSheetByName("Trait Folder Lookup IDs").copyTo(ss).setName("Trait Folder Lookup IDs").hideSheet();
  masterTemplate.getSheetByName("Destination ID Lookup").copyTo(ss).setName("Destination ID Lookup").hideSheet();
  ss.deleteSheet(ss.getSheetByName("Sheet1"));
  
  //delete any Access tokens that were left in the script when publishing the code
  var DocumentProperties  = PropertiesService.getDocumentProperties ();
  DocumentProperties.deleteProperty('AAMaccessToken')
  DocumentProperties.deleteProperty('AAMrefreshToken')
  DocumentProperties.deleteProperty('FBaccessToken')
  ss.getSheetByName("INPUT - Options").getRange(1, 5, 2, 1).clearContent();
  
  //re-write INPUT sheet on install because Gsheets doesnt calculate them when the sheets are copied in order to avoid clicking and entering on every cell manually    
  //Market dropdown
  var DSruleRange = DSIDs.getRange(2, 1, 499, 1)  
  var MarketDropdownRule = SpreadsheetApp.newDataValidation().requireValueInRange(DSruleRange, true).setAllowInvalid(false);
  INPUT.getRange(1, 2).setDataValidation(MarketDropdownRule)
  
  //Brand dropdown
  var BrandRange = BRANDCODES.getRange(2, 1, 4999, 1);
  var BrandDropdownRule = SpreadsheetApp.newDataValidation().requireValueInRange(BrandRange, true).setAllowInvalid(false);
  INPUT.getRange(6, 2, 19, 1).setDataValidation(BrandDropdownRule);
  
  //Product Category dropdowns  
  for (d=0; d<=20; d++) {  
    var ProdCatDropdownRule = SpreadsheetApp.newDataValidation().requireValueInRange( BRANDCODES.getRange(3, 6+d, 26, 1), true).setAllowInvalid(false);
    INPUT.getRange(6+d, 4, 1, 1).setDataValidation(ProdCatDropdownRule);
       }

  
  SpreadsheetApp.getUi().alert("The UC Facebook Automation tool has been installed.\n"+
                               "Run the Add-Ons -> UC Automation -> AAM -> Set AAM Login command to log in to AAM.\n"+
                               "Give access to your media agency and ask them to run the Add-Ons -> UC Automation -> Facebook -> Authorise command to log in to Facebook.\n"+
                               "If you don't see the menus then refresh the page. \n"+
                               "Once both AAM and FB are logged in follow the Guide instructions to use the tool."
                              )
  
}


function addSchedule () {

  var ui = SpreadsheetApp.getUi();
  var result = ui.prompt(
      'Please choose how often the Tracking Sync should run.',
      'Every X days:',
      ui.ButtonSet.OK_CANCEL);

  // Process the user's response and ask for hour.
  var button = result.getSelectedButton();
  var days = parseInt( result.getResponseText() );
  if (button == ui.Button.OK) {
    var result = ui.prompt(
      'Please choose at what time the Tracking Sync should run.',
      'At hour 0 - 24 :',
      ui.ButtonSet.OK_CANCEL);
      var hour = parseInt( result.getResponseText() );
      if (button == ui.Button.OK) {
          var hour = parseInt( result.getResponseText() );
      }
  } 
  
  ScriptApp.newTrigger("GraphAPI").timeBased().everyDays(days).atHour(hour-1).create() ;
  ScriptApp.newTrigger("uploadURLs").timeBased().everyDays(days).atHour(hour).create() ;
  ui.alert("Schedule added");
  
}

function deleteSchedule() {

  var ui = SpreadsheetApp.getUi();
  var triggers = ScriptApp.getProjectTriggers();
  
  for (t=0; t < triggers.length; t++) {
  
    if (triggers[t].getEventType() == "CLOCK") {
 
       ScriptApp.deleteTrigger(trigger[t])
      
    }
  
    ui.alert("All time Sync schedules deleted");
    
  } 
}


function runUpdate() {

  var version = SpreadsheetApp.getActive().getSheetByName("Guide").getRange(62, 2).getValue();
  
  if (version == 0.5) {
  
    return;
    
  } else {
  
   var ui = SpreadsheetApp.getUi();
   ui.alert("The sheet template needs to be updated. Please *save your data* and choose Update Template from the Add-Ons menu."
            + "Please note this operation will delete and replace all sheets and data. The tool might not work if used with the current sheets.") 
    
  }


}