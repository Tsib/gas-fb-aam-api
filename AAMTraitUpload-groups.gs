function createGroupTraits() {

var DocumentProperties  = PropertiesService.getDocumentProperties();

  
  //get a fresh token
  accessToken = getAAMtoken();
  var ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaign Traits");
  ssOutput.getRange(3, 10, 1000, 9).clearContent();
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var dataSourceId =  ssInput.getRange(3, 5).getValue() ;
  var marketName = ssInput.getRange(1, 2).getValue();
  var region = ssInput.getRange(1, 2).getValue();
  var traitLevel = ssInput.getRange(3, 2).getValue();
  var rows = ssOutput.getLastRow();
  var rO = 3;
  
  var data = ssOutput.getRange(3, 1, rows-2, 9).getValues()
  //create an object to store all the groups of AdSets/Ads
  //each object will have a key that is the name of the group and will contain an array with all the rows (columns as arrays) that belong to the group
  var groups = {};
  
  //define all trait group names as keys in the "groups" object to later populate with AdSet IDs
  for (g = 0; g < data.length; g++) {
       
    if ( data[g][0] == "") {
    // if this row doesn't belong to a group then skip
    continue;
    } else if ( typeof(groups[data[g][0]]) == "undefined") {
                        groups[data[g][0]] = {name: data[g][0],
                                              rows: []
                                              }
    }
    //push the row data into the corresponding group
    groups[data[g][0]].rows.push(data[g])
    
       }
  

  //determine if creating AdSet, Campaign or Ad groups (Ads are not given as an option in the UI dropdown but are coded in)
  var IDlocation = 0;
  var ruleKey = "";
  
  if (traitLevel == "AdSet") {
    IDlocation = 4;
    ruleKey = "d_placement";
  } else if (traitLevel == "Ad") {
    IDlocation = 6;
    ruleKey = "d_creative";
  } else if (traitLevel == "Campaign") {
    IDlocation = 2;
    ruleKey = "d_campaign";
   }
  
  

  //loop through list of groups and create Trait definitions

  for (G in Object.keys(groups)) {
     
    var groupObj = groups[Object.keys(groups)[G]];
    
    //if there is only 1 item in the group then break of the current iteration and print an error                        
    if (groupObj.rows.length == 1) {
      ssOutput.getRange(rO, 10).setValue(groupObj.name + " :Groups with a single item in them not allowed. Add at least one more item to this group.");
      continue;
    }
    
    var groupName = groupObj.rows[0][0];
    var brandName = groupObj.rows[0][1];
    var FBcampaignName = groupObj.rows[0][3];
    var folderId = groupObj.rows[0][8];
    var itemID = groupObj.rows[0][IDlocation];
    
    //generate trait name for campaign based on market naming convention
    var traitName = parseCampaignName(region, marketName, brandName, FBcampaignName);
    //append the group name to the trait name
    traitName += "_Audience_" + groupName;
    var traitDescription = "All Clicks from Facebook/Instagram " + traitLevel + " group  "+ groupName
    var traitIntegrationCode = groupName +":"+itemID + "|";
    var traitComments = "Trait created through FAST.";
    var traitRule = '(c_country == "' +marketName+ '" OR d_country == "' +marketName+ '")  AND d_event == "click" AND (c_source CONTAINS "facebook" OR c_source CONTAINS "instagram") AND ('+ruleKey+' == "' + itemID + '"' ;
    
    
    //loop through rows in group to add details of each adset/ad to the trait definition
    for (o=1; o < groupObj.rows.length; o++) {
      
      itemID = groupObj.rows[o][IDlocation];
      traitRule += ' OR '+ruleKey+' == "' + itemID + '"';
      traitIntegrationCode += itemID + "|"
      
    }
    traitRule += ")"
    
    
    var traitDefinition = {
      "name": traitName,
      "description": traitDescription,
      "integrationCode": traitIntegrationCode,
      "comments": traitComments,
      "traitType": "RULE_BASED_TRAIT",
      "status": "ACTIVE",
      "dataSourceId": dataSourceId,
      "folderId": folderId,
      "traitRule": traitRule,
      "categoryId": 0,
      "ttl": "730"
      // ,"type": 1,
      //"pid": 0,
      //"crUID": 0,
      //"upUID": 0,
      //"createTime": 0,
      //"updateTime": 0,
      //"algoModelId": 0,
      //"thresholdValue": 0,
      //"thresholdType": "ACCURACY"
    }
    
    //check whether Trait already exists by Integration Code, skip creation if so and move on to the next row of the loop
    var IClookupResponse = searchTrait( encodeURIComponent(traitIntegrationCode) );
    if (IClookupResponse == 200) {
      ssOutput.getRange(rO, 17).setValue( "---" )
      ssOutput.getRange(rO, 18).setValue( "Trait already exists, skipping" )
      rO++
      continue;
    } 

    
    //make API request
    var options = {'method':'post',
                   'payload': JSON.stringify(traitDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                     options) 
    
    //check response code in case of token expiration and try once more with new token
    if (response.getResponseCode() == 401) {
      accessToken = getAAMtoken();
      var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                       options) 
      } 
    //write results of Trait creation operation to sheet
    var traitID = JSON.parse(response).sid;
    ssOutput.getRange(rO, 10).setValue( traitName );
    ssOutput.getRange(rO, 11).setValue( traitRule );
    ssOutput.getRange(rO, 12).setValue( traitIntegrationCode );
    ssOutput.getRange(rO, 13).setValue( traitDescription );
    ssOutput.getRange(rO, 14).setValue( traitComments );
    ssOutput.getRange(rO, 15).setValue( traitID );
    ssOutput.getRange(rO, 16).setValue( "https://bank.demdex.com/portal/Traits/Traits.ddx#view/" + traitID)
    ssOutput.getRange(rO, 17).setValue(response.getResponseCode())
    ssOutput.getRange(rO, 18).setValue(response.getContentText())
    rO++;
  }
       
       
  
}