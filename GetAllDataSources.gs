//function to pull updated list of Data Source IDs for references across all instances
//This updates the list of data sources in the 2. Master Template sheet, **not this 1. Facebook API Master** one
//The updated references will be installed in a local instance during onboarding and the latest version can be copied over with the Update Template Sheets command - including into this 1. Facebook API Master one

//This function is scheduled to run daily.
function getDSs() {
  
  var ss = SpreadsheetApp.openById("1wKuH3lriy_NOWxFXumkXfVu9wgUzKNuPsYTwLPv8PSY");
  var ssDS = ss.getSheetByName("Data Source IDs");
  var row = 1;
  ssDS.getRange(2, 1, 10000, 7).clearContent();
  
  //get AAM API tokens for all regions
  var tokens = getAAMadminTokens();
  var regions = ["NA", "EMEA", "APAC", "LATAM"];
  
  for (r in regions) { 
  
  var options = {'method':'get',
                     'headers': {'Authorization':'Bearer ' + tokens[regions[r]], 'Content-Type': 'application/json'},
                     'muteHttpExceptions':true } 
      response = UrlFetchApp.fetch("https://api.demdex.com/v1/datasources?search=site&excludeReportSuites=true&sortBy=name", options) 
      var responseCode= parseInt( response.getResponseCode() );
      var APIresponse = response.getContentText();
      var datasources = JSON.parse(response);
      row++
    
    for (d in datasources) {
  
      var market = datasources[d].name.split(" - ")[0];
      var isOffsite = datasources[d].name.split(" - ")[1];
     
      if ( typeof(isOffsite) !== "undefined" && isOffsite.match( "Offsite Data") !== null ) {
      ssDS.getRange(row, 1).setValue(market);
      ssDS.getRange(row, 2).setValue(regions[r]);
      ssDS.getRange(row, 3).setValue(datasources[d].name);
      ssDS.getRange(row, 4).setValue(datasources[d].dataSourceId);
      row++;
      }
     
      
      //close datasource loop
    }
      
    //close regions loop
  }
    
}
