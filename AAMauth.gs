function loginAAM() {

 var ui = SpreadsheetApp.getUi();
  
 var username = ui.prompt("Enter your AAM username - same used with the BAAAM").getResponseText();
 var password = ui.prompt("Enter your AAM password - same used with the BAAAM").getResponseText();

 var DocumentProperties  = PropertiesService.getDocumentProperties ();
 DocumentProperties.setProperty("AAMusername", username);
 DocumentProperties.setProperty("AAMpassword", password);
  
 getAAMtoken();
 
}

function getAAMtoken(region) {
  
  var ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  //var region = ssInput.getRange(2, 2).getValue();
  var DocumentProperties  = PropertiesService.getDocumentProperties ();
  
  var username = DocumentProperties.getProperty("AAMusername");
  var password = DocumentProperties.getProperty("AAMpassword");
      
  //global credentials with access to everything. only for testing.
  /*
  var username = "";
  var password = "Momo1792$adobe";
  
switch(region) {
    case "EMEA":
        username = "morgane_unilever_api";
        break;
    case "APAC":
        username = "morgane_unilever3_api" ;
        break;
    case "NA":
        username = "morgane_unilever2_api";
        break;
    case "LATAM":
        username = "morgane_unileverlatam_api";
        break;
}
  */

   var options = {'method':'post', 
                  'payload' : 'grant_type=password&username='+username+'&password='+password,
                  'headers': {'Authorization':'Basic dW5pbGV2ZXItYmFhYW06bmw0ZTRnNWhuYjd2Z2RocnI3MWxtamRlMnJzaHFvbWV2cDlxbW1vcHZwbnNmcDJhZnUz',
                              'Content-Type' : 'application/x-www-form-urlencoded' 
                             }
                 }

   var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/oauth/token",
                      options) )
  
   
  
  var accessToken = response.access_token;
  var refreshToken = response.refresh_token;
  
  
  DocumentProperties.setProperty('AAMaccessToken', accessToken);
  DocumentProperties.setProperty('AAMrefreshToken', refreshToken);

  ssInput.getRange(2, 5).setValue(username);

  return(accessToken);

  
}


//this function is used to get a token for each AAM instance using the global D+A morgane_unilever_XX accounts
//those tokens are used to refresh the Data Source and Folder reference sheets in the master template GSheet only on a daily trigger.
function getAAMadminTokens() {

  var username = "";
  var password = "Momo1792$adobe";
  var tokens = {};
  
  //array of usernames for EMEA, APAC, NA, LATAM
  var regions = ["EMEA", "APAC", "NA", "LATAM"];
  var usernames = ["morgane_unilever_api", "morgane_unilever3_api" ,"morgane_unilever2_api", "morgane_unileverlatam_api"]

  
  for (u =0; u<4; u++) {
   var options = {'method':'post', 
                  'payload' : 'grant_type=password&username='+usernames[u]+'&password='+password,
                  'headers': {'Authorization':'Basic dW5pbGV2ZXItYmFhYW06bmw0ZTRnNWhuYjd2Z2RocnI3MWxtamRlMnJzaHFvbWV2cDlxbW1vcHZwbnNmcDJhZnUz',
                              'Content-Type' : 'application/x-www-form-urlencoded' 
                             }
                 }

   var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/oauth/token",
                      options) )
  
   tokens[regions[u]] = response.access_token;
}
  
  return tokens;

}