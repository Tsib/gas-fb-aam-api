function updateTemplate() {
  
  //get master template Spreadsheet file
  file = SpreadsheetApp.openById('1aWFfdMlZdMMIQmoi3d6UcEM6MosS4w8epLexorCNFNQ');
  //get user's current Spreadsheet file
  ss = SpreadsheetApp.getActive();
  
  var guide = ss.getSheetByName("Guide");
  var input  = ss.getSheetByName("INPUT - Options");
  var output = ss.getSheetByName("OUTPUT - Ad URLs");
  var outputCampaign = ss.getSheetByName("OUTPUT - Campaign Traits");
  var codes = ss.getSheetByName("Brand & ProdCat codes");
  var DSs = ss.getSheetByName("Data Source IDs");
  var folders  = ss.getSheetByName("Trait Folder Lookup IDs");
  
  //save config data from Input sheet
  var market = input.getRange(1, 2).getValue();
  var FBuser = input.getRange(1, 5).getValue();
  var AAMuser = input.getRange(2, 5).getValue();
  var localOperator = input.getRange(1, 8).getValue();
  
  //remove out of date sheets from current spreadsheet
  //copy updated sheets from master template
  //alternate between delete and create as we can't delete all sheets and end up with 0!
  
  //check that sheets exist in case of initial set up or deletion, otherwise deleteSheet() will end in error
  //rename copied sheets as they are created with name "Copy Of X"
  
  if (guide !== null) {  ss.deleteSheet(guide) ; }
  file.getSheetByName("Guide").copyTo(ss).setName("Guide");
    
  if (input !== null) {  ss.deleteSheet(input) ; }
  file.getSheetByName("INPUT - Options").copyTo(ss).setName("INPUT - Options");
  
  if (output !== null) { ss.deleteSheet(output); }
  file.getSheetByName("OUTPUT - Campaigns & Traits").copyTo(ss).setName("OUTPUT - Campaigns & Traits");
  
  if (outputCampaign !== null) { ss.deleteSheet(output); }
  file.getSheetByName("OUTPUT - Campaign Traits").copyTo(ss).setName("OUTPUT - Campaign Traits");
    
  if (codes !== null) { ss.deleteSheet(codes); }
  file.getSheetByName("Brand & ProdCat codes").copyTo(ss).setName("Brand & ProdCat codes").hideSheet();
    
  if (DSs !== null) {  ss.deleteSheet(DSs) ; }
  file.getSheetByName("Data Source IDs").copyTo(ss).setName("Data Source IDs").hideSheet();
    
  if (folders !== null) {  ss.deleteSheet(folders) ; }
  file.getSheetByName("Trait Folder Lookup IDs").copyTo(ss).setName("Trait Folder Lookup IDs").hideSheet();
  
  //re-enter config information into the new empty sheets
  input.getRange(1, 2).setValue(market);
  input.setRange(1, 5).getValue(FBuser);
  input.setRange(2, 5).getValue(AAMuser);
  input.setRange(1, 8).getValue(localOperator);
  
  ss.toast("The latest sheets have been copied.", "Template update complete", 3)
  
  
}


//function to only update the 3 ref shees
function updateRefs() {

  //get master template Spreadsheet file
  file = SpreadsheetApp.openById('1aWFfdMlZdMMIQmoi3d6UcEM6MosS4w8epLexorCNFNQ');
  //get user's current Spreadsheet file
  ss = SpreadsheetApp.getActive();
  
  var DSs = ss.getSheetByName("Data Source IDs");
  var folders  = ss.getSheetByName("Trait Folder Lookup IDs");
  var codes = ss.getSheetByName("Brand & ProdCat codes");
  
  
  //remove out of date sheets from current spreadsheet
  //copy updated sheets from master template
  //alternate between delete and create as we can't delete all sheets and end up with 0!
  
  //check that sheets exist in case of initial set up or deletion, otherwise deleteSheet() will end in error
  //rename copied sheets as they are created with name "Copy Of X"
  
  if (DSs !== null) {  ss.deleteSheet(DSs) ; }
  file.getSheetByName("Data Source IDs").copyTo(ss).setName("Data Source IDs").hideSheet();
    
  if (folders !== null) {  ss.deleteSheet(folders) ; }
  file.getSheetByName("Trait Folder Lookup IDs").copyTo(ss).setName("Trait Folder Lookup IDs").hideSheet();
  
  if (codes !== null) { ss.deleteSheet(codes); }
  file.getSheetByName("Brand & ProdCat codes").copyTo(ss).setName("Brand & ProdCat codes").hideSheet();
  
  ss.toast("The latest references have been copied.", "References update complete", 3)
  

}