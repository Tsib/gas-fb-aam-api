function AAMsegments() {
  
  accessToken = getAAMtoken();
  
  ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaigns & Traits");
  var dataSourceId =  ssInput.getRange(3, 5).getValue() ;
var traitLevel = ssInput.getRange(2, 2).getValue();
  var segmentParentFolderId =  ssInput.getRange(3, 12).getValue() ; 
  
  
  ssMemory = SpreadsheetApp.getActive().getSheetByName("hidden memory-DONT EDIT");
  var rows = ssMemory.getLastRow();
  var columns = ssMemory.getLastColumn();
  
  var campaignName = "";
  var campaignID = "";
  
  
  //loop through campaigns
  for (c= 1; c<= columns; c++ ) {
    
    var segmentRule =  "";
    campaignName = ssMemory.getRange(1, c).getValue().split("|")[0];
    campaignID = ssMemory.getRange(1, c).getValue().split("|")[1];
    
       //loop through placements
    for (p = 2; p <= rows; p++ ) {
         
      segmentRule += ssMemory.getRange(p, c).getValue() + "T";   
      //add OR if more traits are left to be added to segment
    (p !== rows) ? segmentRule+= " OR " : false;
      
         }
       
       Logger.log(segmentRule);
       
  
  //compile segment definition for campaign
  var segmentDefinition = {
  "name": campaignName,
  "description": campaignName,
  "integrationCode": campaignID,
  "segmentRule": segmentRule,
  "folderId": segmentParentFolderId,
  "dataSourceId": dataSourceId
  //, "mergeRuleDataSourceId": 
}
  
    //make API request
    var options = {'method':'post',
                   'payload': JSON.stringify(segmentDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':true } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/segments",
                                   options) 
  ssOutput.getRange(2 + c, 28).setValue(response.getResponseCode())
   ssOutput.getRange(2 + c, 29).setValue(response.getContentText())
    Logger.log(response)
    
  if (response.getResponseCode() == 200 || response.getResponseCode() == 201) {
      //ssOutput.getRange(2 + c, 14).setValue( segmentParentFolderId );
      ssOutput.getRange(2 + c, 23).setValue( campaignName );
      ssOutput.getRange(2 + c, 24).setValue( segmentRule );
      ssOutput.getRange(2 + c, 25).setValue( campaignID );
      ssOutput.getRange(2 + c, 26).setValue( campaignName );
    //  ssOutput.getRange(2 + c, 19).setValue( traitComments );
  }
   
   //close campaign FOR loop 
  }
  
}
