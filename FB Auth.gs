function getFBservice(scope) {
  // Create a new service with the given name. The name will be used when
  // persisting the authorized token, so ensure it is unique within the
  // scope of the property store.
 
  return OAuth2.createService('fb')

      // Set the endpoint URLs, which are the same for all Google services.
      .setAuthorizationBaseUrl('https://www.facebook.com/v3.2/dialog/oauth')
      .setTokenUrl('https://graph.facebook.com/v3.2/oauth/access_token')

      // Set the client ID and secret, from the Google Developers Console.
      .setClientId('1360561797313389')
      .setClientSecret('f481bdccd3075e7eadbf49e0f8bfb5d8')

      // Set the name of the callback function in the script referenced
      // above that should be invoked to complete the OAuth flow.
      .setCallbackFunction('authCallbackFB')

      // Set the property store where authorized tokens should be persisted.
      .setPropertyStore(PropertiesService.getUserProperties())

      // Set the scopes to request (space-separated for Google services).
      .setScope('ads_management')
      //.setParam('response_type', 'token')

}


function showSidebar() {
  var FBservice = getFBservice();
  if (!FBservice.hasAccess()) {
    var authorizationUrl = FBservice.getAuthorizationUrl();
    var template = HtmlService.createTemplate(
        '<a href="<?= authorizationUrl ?>" target="_blank">Click here</a>' +
        ' to authorise the automation tool with a FB login. <p>For Edit access to upload tagged Ad URLs the FB login must have <b>Advertiser</b> access to the Ad Accounts. Otherwise Analyst access is OK</p>'
        + '<p>Edit permissions will be requested by the app, but unless the FB account is an Advertiser ad editing will not be possible. </p>');
    template.authorizationUrl = authorizationUrl;
    var page = template.evaluate();
    SpreadsheetApp.getUi().showSidebar(page);
  } else {
    var userName = SpreadsheetApp.getActive().getSheetByName("INPUT - Options").getRange(1, 5).getValue();
    SpreadsheetApp.getActive().toast("The tool is already logged in to the FB API as "+userName, "Already authorised", 5)
  }
}


function authCallbackFB(request) {
  var FBservice = getFBservice();
  var ui = SpreadsheetApp.getUi()
  var isAuthorized = FBservice.handleCallback(request);
  if (isAuthorized) {
    //save access token in script properties memory
    var DocumentProperties  = PropertiesService.getDocumentProperties();
    FBaccessToken = FBservice.getAccessToken();
    DocumentProperties.setProperty('FBaccessToken', FBaccessToken );
    var options = {'headers': {'Authorization':'Bearer ' + FBaccessToken }, 'muteHttpExceptions':false}
    var userName =   JSON.parse( UrlFetchApp.fetch("https://graph.facebook.com/v3.2/me", options) ).name;
    SpreadsheetApp.getActive().getSheetByName("INPUT - Options").getRange(1, 5).setValue(userName)
    
    Logger.log(FBaccessToken)
    
    return HtmlService.createHtmlOutput('Success! You can close this tab.');
  
  } else {
    return HtmlService.createHtmlOutput('Denied. You can close this tab');
  }
}


function FBlogout() {
  var service = getFBservice()
  service.reset();
  PropertiesService.getDocumentProperties().deleteProperty("FBaccessToken");
  SpreadsheetApp.getActive().getSheetByName("INPUT - Options").getRange(1, 5).clearContent();
}

