//Function to upload tagged URL by editing the URL field of the AdCreative and then Updating the adcreative object in the Ad (not the AdCreative node directly)
//https://developers.facebook.com/docs/marketing-api/reference/adgroup#Updating
function uploadURL(adID, adCreative, r, e) {
  
  ssOutput = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Ad URLs");
  var rO = ssOutput.getLastRow();
  var DocumentProperties  = PropertiesService.getDocumentProperties();
  FBaccessToken = DocumentProperties.getProperty("FBaccessToken");
    
  //if the script is ran manually from the menu then read data from the sheet
  if(typeof(e) == "undefined") {
  
    for (r=3; r<=rO; r++) {
      
    var taggedURL = ssOutput.getRange(r, 12).getValue();
     
      //if there is no URL to upload skip to the next row
      if(taggedURL == "") {continue}  
      
    var adID = ssOutput.getRange(r, 6).getValue();
    var adType = ssOutput.getRange(r, 16).getValue();
    var adCreative = JSON.parse( ssOutput.getRange(r, 17).getValue() );
    
      //overwrite the link in ObjectStorySpec with the URL listed in the sheet (in case it has been edited by the user to correct an error)
      switch(adType) {
    case "image":
       adCreative.object_story_spec.link_data.link = taggedURL
        break;
        case "carousel":
          //update URL for each carousel card
          var taggedURLs = taggedURL.split("::");
          var cards = taggedURLs.length;
          //if the carousel includes an optional See More card (which is in another object), tag that from the last URL in the list and reduce the length of the card URLs list by 1.
          if ( typeof(adCreative.object_story_spec['link_data'].link) !== "undefined") {
          adCreative.object_story_spec['link_data'].link = taggedURLs[cards-1]
          cards--
          }
          for (C = 0; C < cards; C++) {
         adCreative.object_story_spec['link_data'].child_attachments[C].link = taggedURLs[C];
               }
          
        break;
    case "video":
        adCreative.object_story_spec.video_data.call_to_action.value.link = taggedURL
        break;
    case "multiAsset":
        adCreative.asset_feed_spec.link_urls[0].website_url = taggedURL
        break;            
    default:
        break;
    }
      
    
    var body = {
	"creative": adCreative
   }
  
  var options = {'method':'POST', 'payload': JSON.stringify(body), 'headers': {'Authorization':'Bearer ' + FBaccessToken, 'Content-Type':'application/json' }, 'muteHttpExceptions':false}
  var response = UrlFetchApp.fetch("https://graph.facebook.com/v3.2/" + adID + "?redownload=true", options);
  var code = response.getResponseCode()

  if (code == 200) {
  ssOutput.getRange(r, 13).setValue("Upload successful");
  } else {
  ssOutput.getRange(r, 13).setValue("Upload error: "+response.getContentText());
  }
      
      }
      
  } else {
  //code for when function is called through schedule
  
  }
  
  
  
    
}
