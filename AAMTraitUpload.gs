function searchTrait(traitIntegrationCode) {

    var options = {'method':'get',
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/traits/ic:" + traitIntegrationCode,
                                 options)  
    
    var responseCode = response.getResponseCode();
    return responseCode;

}



function AAMtraits() {
  
  var DocumentProperties  = PropertiesService.getDocumentProperties();

  //get AAM API authorisation token
  accessToken = getAAMtoken();
 
  ssInput = SpreadsheetApp.getActive().getSheetByName("INPUT - Options");
  var dataSourceId =  ssInput.getRange(3, 5).getValue() ;
  var marketName = ssInput.getRange(1, 2).getValue();
  var region = ssInput.getRange(1, 2).getValue() ;
  var traitLevel = ssInput.getRange(3, 2).getValue();
  
  ssOutputCampaign = SpreadsheetApp.getActive().getSheetByName("OUTPUT - Campaign Traits");
  var rows = ssOutputCampaign.getLastRow();

  
  for (var r=3; r<=rows; r++) {
    
    //read FB campaign details
    var brandName = ssOutputCampaign.getRange(r, 2).getValue()
    var campaignID = ssOutputCampaign.getRange(r, 3).getValue();
    var FBcampaignName = ssOutputCampaign.getRange(r, 4).getValue();
    var folderId = ssOutputCampaign.getRange(r, 9).getValue();


    //define trait details
    //check if operating on Placement or Creative level and adjust definitions
    if (traitLevel == "Campaign") {
    
      var traitName = parseCampaignName(region, marketName, brandName, FBcampaignName);
      var traitDescription = "All Clicks from Facebook/Instagram Campaign "+FBcampaignName;
      var traitIntegrationCode = campaignID;
      var traitComments = "Trait created through FAST.";
      var traitRule = 'd_campaign == "' + campaignID + '" AND (c_country == "' +marketName+ '" OR d_country == "' +marketName+ '") AND d_event == "click" AND (c_source CONTAINS "facebook" OR c_source CONTAINS "instagram")' ;
    
    }
    else if (traitLevel == "Placement") {
    
      var ui = SpreadsheetApp.getUi();
    ui.alert("Individual Adset-level traits not supported. If you want to create grouped traits use the Create Group Traits command");
    return;
       
      /* //commenting out code as placement-level traits shouldn't be created in bulk with the tool
      var adsetID = ssOutputCampaign.getRange(r, 4).getValue();
      var adsetName = ssOutputCampaign.getRange(r, 5).getValue();
      var traitName = PARSEADSETNAME()
      var traitDescription = "All Clicks from Facebook/Instagram placement  "+adsetName +" in Campaign "+FBcampaignName;
      var traitIntegrationCode = adsetID;
      var traitComments = "Trait automatically created by Automation Tool.";
      var traitRule = 'd_placement == "' + adsetID + '" AND (c_country == "' +marketName+ '" OR d_country == "' +marketName+ '") AND d_event == "click" AND (c_source CONTAINS "facebook" OR c_source CONTAINS "instagram")' ;
      */
      
    } else if (traitLevel == "Creative") {
    
    var ui = SpreadsheetApp.getUi();
    ui.alert("Individual Ad-level traits not supported. If you want to create grouped traits use the Create Group Traits command");
    return;
    
      /* //commenting out code as creative-level traits shouldn't be created in bulk with the tool
    var adsetID = ssOutputCampaign.getRange(r, 4).getValue();
    var adsetName = ssOutputCampaign.getRange(r, 5).getValue();
    var adID = ssOutputCampaign.getRange(r, 6).getValue();
    var adName = PARSEADNAME()
    var destinationURL = ssOutputCampaign.getRange(r, 9).getValue();
    var imageURL = ssOutputCampaign.getRange(r, 8).getValue();
    var traitName = marketName + "_" + brandName + "_SocialFacebook_" + campaignName + "|" + adsetName + "|" + adName;
    var traitDescription = "Ad ID " +adID+ ", Adset ID "+adsetID+", Campaign ID "+FBcampaignName;
    var traitIntegrationCode = adID;
    var traitComments = "Trait automatically created by Automation Tool. Asset: "+imageURL +". URL: " + destinationURL;
    var traitRule = 'd_creative == "' + adID + '" AND (c_country == "' +marketName+ '" OR d_country == "' +marketName+ '")  AND d_event == "click" AND (c_source CONTAINS "facebook" OR c_source CONTAINS "instagram")' ;
    */ 
    
    } else {
    var ui = SpreadsheetApp.getUi();
    ui.alert("For this command the Trait Level must be set to Campaign.");
    return;
    }
   
    var traitDefinition = {
      "name": traitName,
      "description": traitDescription,
      "integrationCode": traitIntegrationCode,
      "comments": traitComments,
      "traitType": "RULE_BASED_TRAIT",
      "status": "ACTIVE",
      "dataSourceId": dataSourceId,
      "folderId": folderId,
      "traitRule": traitRule,
      "categoryId": 0,
      "ttl": "730"
      // ,"type": 1,
      //"pid": 0,
      //"crUID": 0,
      //"upUID": 0,
      //"createTime": 0,
      //"updateTime": 0,
      //"algoModelId": 0,
      //"thresholdValue": 0,
      //"thresholdType": "ACCURACY"
    }
    
    //check whether Trait already exists by Integration Code, skip creation if so and move on to the next row of the loop
    var IClookupResponse = searchTrait(traitIntegrationCode);
    
    if (IClookupResponse == 200) {
      ssOutputCampaign.getRange(r, 17).setValue( "---" )
      ssOutputCampaign.getRange(r, 18).setValue( "Trait already exists, skipping" )
      continue;
    } 
    
       
      //make API request
    var options = {'method':'post',
                   'payload': JSON.stringify(traitDefinition),
                   'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
                   'muteHttpExceptions':false } 
    var response = UrlFetchApp.fetch("https://api.demdex.com/v1/traits",
                                   options) 

  //check response code in case of token expiration and try once more with new token
  if (response.getResponseCode() == 200 || response.getResponseCode() == 201) {
    
    var traitID = JSON.parse(response).sid;
    
      ssOutputCampaign.getRange(r, 10).setValue( traitName );
      ssOutputCampaign.getRange(r, 11).setValue( traitRule );
      ssOutputCampaign.getRange(r, 12).setValue( traitIntegrationCode );
      ssOutputCampaign.getRange(r, 13).setValue( traitDescription );
      ssOutputCampaign.getRange(r, 14).setValue( traitComments );
      ssOutputCampaign.getRange(r, 15).setValue( traitID );
      ssOutputCampaign.getRange(r, 16).setValue( "https://bank.demdex.com/portal/Traits/Traits.ddx#view/" + traitID)
      ssOutputCampaign.getRange(r, 17).setValue(response.getResponseCode())
      ssOutputCampaign.getRange(r, 18).setValue(response.getContentText())
  }


  }
}



//WON'T due to Folder Trait limitations
function createFolder(parentFolderId,level,name) {

  //Create Campaign Folder
  var folderDefinition = {
    //"pid": 0,
    "parentFolderId": parentFolderId,
    "name": level + ": " + name
  }
    
    var options = {'method':'post',
    'payload': JSON.stringify(folderDefinition),
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits",
                                 options)  )

    // check if folder name already exists
    if (response.code == "NAME_TAKEN") {
    return searchFolder(parentFolderId,name)
    } else { return {"folderId": response.folderId, "folderName" : response.name}; }

}

//WON'T USE due to Folder Trait limitations
function searchFolder(parentFolderId,campaignName) {
   //if campaign folder is already created under Parent folder, look for its folder ID
   var options = {'method':'get',
    'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response = JSON.parse( UrlFetchApp.fetch("https://api.demdex.com/v1/folders/traits/"+parentFolderId+"?includeSubFolders=true",
                                 options)  )
    for (var f=0; f < response.subFolders.length; f++) {
      if (response.subFolders[f].name.match(campaignName) !== null) {
      return response.subFolders[f].folderId;
      }   
      }
}
