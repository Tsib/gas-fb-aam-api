//function to create Custom Audiences in FB Ads Manager from mapped segments in the AAM Destination using the AAM Mapping ID as the CA Pixel Event value
//https://developers.facebook.com/docs/marketing-api/facebook-pixel/website-custom-audiences/

function getDestinationMappings() {
  
  var ss = SpreadsheetApp.getActive().getSheetByName("AAM Mapping-FB Audience Sync");
  var destinationID = ss.getRange(2, 2).getValue();
  var rows = ss.getLastRow();
  accessToken = getAAMtoken();
  
  
  //pull mappings on the market FB Destination from the API
   var options = {'method':'get',
      'headers': {'Authorization':'Bearer ' + accessToken, 'Content-Type': 'application/json'},
      'muteHttpExceptions':true } 
    var response =  UrlFetchApp.fetch("https://api.demdex.com/v1/destinations/" + destinationID + "/mappings",
                                 options)  
    
    var mappings = JSON.parse(response)
    //transform the mappings object to only keep the values we need
    mappedSegments = mappings.map( function(seg) { return [seg.elementName, seg.elementDescription, seg.traitAlias, seg.startDate] } );
    
    //write all mappings to sheet, including duplicate mappings that were already listed
    ss.getRange(rows+1, 1, mappings.length, 4).setValues(mappedSegments);
     
  //remove duplicate rows, keeping old rows with FB Audience creation results.
  var rows2 = ss.getLastRow();
  //sort by rows that have FB Audience results so the the new rows get deleted
  ss.getRange(7,1, rows2-7, 7).sort(6);
  var dataRange = ss.getRange(7,1, rows2-7, 4);
  var data = dataRange.getValues();
  var newData = [];
  for (var i in data) {
    var row = data[i];
    var duplicate = false;
    for (var j in newData) {
      if (row.join() == newData[j].join()) {
        duplicate = true;
      }
    }
    if (!duplicate) {
      newData.push(row);
    }
  }
  dataRange.clearContent();
  ss.getRange(7, 1, newData.length, newData[0].length).setValues(newData);
  
}


function syncSegments() {

  var ss = SpreadsheetApp.getActive().getSheetByName("AAM Mapping-FB Audience Sync");
  var market = ss.getRange(1, 2).getValue();
  var AdAccount = ss.getRange(3, 2).getValue();
  var pixelID = ss.getRange(4, 2).getValue();
  var rows = ss.getLastRow();
  var DocumentProperties  = PropertiesService.getDocumentProperties ();
  FBaccessToken = DocumentProperties.getProperty("FBaccessToken");
  
  var segments = ss.getRange(7, 1, rows-7, 7).getValues();
  
  //loop through the Audience rows and check which ones without an existing audience should have one created 
  for (var r=0; r<segments.length; r++) {
       
    var createAudience = segments[r][4]; 
    var audienceName =  segments[r][6];
    var segmentName = segments[r][0]; 
    var segmentAlias = segments[r][2]; 
    var description = segments[r][1];
    
    if (createAudience == "YES" && audienceName == "") {
    
      //put together the FB Audience definition using the segment mapping ID
      var body = {
        "description": description,
        "rule":{
        "inclusions": {
          "operator": "or",
          "rules": [
            {
              "event_sources": [
                {
                  "id": pixelID,
                  "type": "pixel"
                }
              ],
              //180 days retention - longest possible
              "retention_seconds": 15552000,
              "filter": {
                "operator": "and",
                "filters": [
                  {
                    "field": "event",
                    "operator": "eq",
                    "value": "Adobe-Audience-Manager-Segment"
                  },
                  {"operator":"or",
                   "filters": [
                     {
                       "field": "segID",
                       "operator": "i_contains",
                       "value": segmentAlias
                     }
                     ]
                  }
                ]
              }
            }
          ]
        }
      }
                 }
      
      var options = {'method':'POST',
                     'payload': JSON.stringify(body),
                     'headers': {'Authorization':'Bearer ' + FBaccessToken, 'Content-Type':'application/json' },
                     'muteHttpExceptions':true}
      var response = UrlFetchApp.fetch("https://graph.facebook.com/v3.2/act_" + AdAccount + "/customaudiences?prefill=true&name="+segmentName, options);
      var code = response.getResponseCode()
      
Logger.log(response)
      
      ss.getRange(r+7, 6).setValue(code);
      ss.getRange(r+7, 7).setValue(segmentName);
      ss.getRange(r+7, 8).setValue(new Date() );
      
    }
    
    
       }
  


}