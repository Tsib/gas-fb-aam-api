//function to generate AAM(+UTM) tagged URL with customisations for any market that requires them
function tagURL(region, market, adType, destinationURL, campaignName, gender, ageMin, ageMax, prodCatCode, brandCode, destinationType, FB_IG) {

  var protocol = "http"
  
     switch(region) {
    case "EMEA":
      AAMdomain = "unilever";
        break;
        case "NA":
       AAMdomain = "unilever2";
        break;
    case "APAC":
       AAMdomain = "unilever3";
        break;
    case "LATAM":
       AAMdomain = "unileverbrazil";
        break;
     }
        
  //default value for c_source if parameter is missing
  ( FB_IG == "" ) ? FB_IG = "facebook_instagram" : null;
  
        var UTMtag = "utm_medium=display"
        + "&utm_source=" + FB_IG
        //in the utm_campaign parameter we put in the campaign name instead of the {{campaign.name}} macro because the landing url needs to be encoded.
        //if the macro is encoded FB won't see it and dynamically replace it, if the macro is put unencoded for FB to populate, the name will be unencoded in the encoded URL which might break it.
        + "&utm_campaign=" + campaignName
        + "&utm_term=" + gender + ageMin + "-" + ageMax
        + "&utm_content=" + adType

        //generate UTM parameters if not already present and if the campaign goes to a UL site (based on user setting, not URL domain)
        if ( destinationURL.indexOf("utm_") < 0 && destinationType == "unilever") {
        //append UTM to the the destination URL depending on whether a query string already exists
        ( destinationURL.indexOf("?") > 0 ) ? destinationURL+="&"+UTMtag : destinationURL+="?"+UTMtag;
        }
        
        //determing whether landing page uses SSL (mixing protocol in the AAM and the landing URLs causes problems)
        if ( destinationURL.indexOf("https") > 0 ) { protocol = "https" }
        
      
        var taggedURL =  protocol + "://"+AAMdomain+".demdex.net/event?d_event=click"
        + "&d_bu="  + ssInput.getRange(3, 5).getValue()
        + "&c_medium=social"
        + "&c_source=" + FB_IG //optionally set in Input sheet on campaign level until FB allows distinction of the two
        + "&c_prodcat=" + prodCatCode
        + "&c_brandcode=" + brandCode
        + "&c_country=" + market
        + "&c_contenttype=" + adType 
        + "&d_campaign={{campaign.id}}"
        + "&d_placement={{adset.id}}"
        + "&d_creative={{ad.id}}" 
        + "&d_adgroup=" + gender + ageMin + "-" + ageMax + "_" + "{{adset.name}}"
        + "&c_destination=" + destinationType
        + "&d_rd=" + encodeURIComponent(destinationURL);
       
      return  taggedURL;

}