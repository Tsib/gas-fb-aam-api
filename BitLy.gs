//OAuth2 authorisation flow - not using, wasnt working
function getBitLyservice() {
  // Create a new service with the given name. The name will be used when
  // persisting the authorized token, so ensure it is unique within the
  // scope of the property store.
 
  return OAuth2.createService('bitly')
nt
      // Set the endpoint URLs, which are the same for all Google services.
      .setAuthorizationBaseUrl('https://bitly.com/oauth/authorize')
      .setTokenUrl('https://bitly.com/oauth/access_token')

      // Set the client ID and secret
      .setClientId('')
      .setClientSecret('')

      // Set the name of the callback function in the script referenced
      // above that should be invoked to complete the OAuth flow.
      .setCallbackFunction('authCallbackBitLy')

      // Set the property store where authorized tokens should be persisted.
      .setPropertyStore(PropertiesService.getUserProperties())

      // Set the scopes to request (space-separated for Google services).
      //.setScope('')

}


function BitLyshowSidebar() {
  var bitLyservice = getBitLyservice();
  if (!bitLyservice.hasAccess()) {
    var authorizationUrl = bitLyservice.getAuthorizationUrl();
    var template = HtmlService.createTemplate(
        '<a href="<?= authorizationUrl ?>" target="_blank">Authorize</a>. ' +
        'Reopen the sidebar when the authorization is complete.');
    template.authorizationUrl = authorizationUrl;
    var page = template.evaluate();
    SpreadsheetApp.getUi().showSidebar(page);
  } else {
  // ...
  }
}


function authCallbackBitLy(request) {
 var bitLyservice = getBitLyservice();
  var isAuthorized = bitLyservice.handleCallback(request);
  if (isAuthorized) {
    //save access token in script properties memory
    var DocumentProperties  = PropertiesService.getDocumentProperties();
    DocumentProperties.setProperty('BitLyAccessToken', bitLyservice.getAccessToken() );
    
    return HtmlService.createHtmlOutput('Success! You can close this tab.');
  
  } else {
    return HtmlService.createHtmlOutput('Denied. You can close this tab');
  }
}


function BitLylogout() {
  var service = bitLyservice()
  service.reset();
}


function expandBitly(shortURL) {

  //authorisation hardcoded as an access token from a username/password grant rather than OAuth2 flow
  var options = {'headers': {'Authorization':'Bearer ' + 'aa173ec9053f60a8a4d8cb9ef409ed5ea5a38a6c' , 'Content-Type': 'application/json'}, 
                 'payload' : JSON.stringify( {"bitlink_id": shortURL.split("//")[1]} ),
                 'muteHttpExceptions':false  
                }

  var response = UrlFetchApp.fetch("https://api-ssl.bitly.com/v4/expand", options);

  return JSON.parse(response).long_url;

}

function createBitly() {

  var longURL = "http://unilever.demdex.net/event?d_event=click&d_bu=DE&c_medium=social&c_source=facebook&c_campaignname=L_B&J_FAST_Event CA+Deliveroo_Jul2018 [10001346]&c_prodcat=CF1205&c_brandcode=BF0543&c_country=DE&c_contenttype=ad&d_adgroup=6141683134813&c_destination=unilever&d_rd=https%3A%2F%2Fdeliveroo.de%2Fde%2Fmarken%2Fben-und-jerrys-magnum%3Futm_source%3Dfacebook_ul%26utm_medium%3Dcpc%26utm_campaign%3Dbenjerry%26utm_content%3DHHnoCA";
  //authorisation hardcoded as an access token from a username/password grant rather than OAuth2 flow
  var options = {'headers': {'Authorization':'Bearer ' + 'aa173ec9053f60a8a4d8cb9ef409ed5ea5a38a6c' , 'Content-Type': 'application/json'}, 
                 'payload' : JSON.stringify( 
                   { //"domain": "bit.ly",
                     // "title": "string",
                     "long_url": longURL                    
                   }
                 ),
                 'muteHttpExceptions':false  
                }

  var response = UrlFetchApp.fetch("https://api-ssl.bitly.com/v4/bitlinks", options);

  //Logger.log(response);
  return JSON.parse(response).link ;

}